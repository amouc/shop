package com.amou.shop

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        button_signup.setOnClickListener {
            val sEmail = ed_email.text.toString()
            val sPassword = ed_password.text.toString()
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(sEmail,sPassword)
                //非同步處理 需連網 不會等待完成 才執行後面程式
                .addOnCompleteListener {
                    //需判斷是否建立成功
                    if(it.isSuccessful){
                        AlertDialog.Builder(this)
                            .setTitle("Sign Up")
                            .setMessage("Account created")
                            .setPositiveButton("OK"){dialog, which ->
                                //傳回result code
                                setResult(Activity.RESULT_OK)
                                //回上一個activity
                                finish()
                            }
                            .show()
                    }else{
                        AlertDialog.Builder(this)
                            .setTitle("Sign Fail")
                                //會告知失敗原因
                            .setMessage(it.exception?.message)
                            .setPositiveButton("OK",null)
                            .show()
                    }
                }
        }
    }
}
