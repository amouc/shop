package com.amou.shop

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_parking.*
import org.jetbrains.anko.*
import java.net.URL

class ParkingActivity : AppCompatActivity(), AnkoLogger {
   val TAG = ParkingActivity::class.java.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parking)
        val parking = "http://data.tycg.gov.tw/opendata/datalist/datasetMeta/download?id=f4cc0b12-86ac-40f9-8745-885bddc18f79&rid=0daad6e6-0632-44f5-bd25-5e1de1e9146f"

        //anko
        //另一個執行緒
        doAsync {
            val url = URL(parking)
            //Manifest 裡面要加INTERNET權限  要加 android:usesCleartextTraffic="true"   另外用AsyncTask 用另一個執行緒連網
            val json = url.readText()
            info(json)
            //Main Thread UI 設定互動只能在Main Thread 執行
            uiThread {
                //Toast.makeText(it,"Got it",Toast.LENGTH_LONG).show()
                toast("Got it")
                //將資料讀進元件
                text_info.text = json
                alert("Got it","ALERT"){
                    okButton {
                        paresGson(json)
                    }
                    cancelButton {  }
                }.show()
            }
        }
        //ParkingTask().execute(parking)
    }

    private fun paresGson(json: String) {
        val parking =Gson().fromJson<Parking>(json,Parking::class.java)
        info(parking.parkingLots.size)
        parking.parkingLots.forEach {
            info { "${it.areaId}  ${it.areaName}  ${it.parkName}  ${it.totalSpace}"}
        }
    }

    inner class ParkingTask: AsyncTask<String,Void,String>(){
        //另一個執行緒
        override fun doInBackground(vararg params: String?): String {
            val url = URL(params[0])
            //Manifest 裡面要加INTERNET權限  要加 android:usesCleartextTraffic="true"   另外用AsyncTask 用另一個執行緒連網
            val json = url.readText()
            Log.d(TAG, "doInBackground:$json ")

            return json
        }
        //Main Thread UI 設定互動只能在Main Thread 執行
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            Toast.makeText(this@ParkingActivity,"Got it",Toast.LENGTH_LONG).show()
            text_info.text = result
        }
    }
}

/*

*/


data class Parking(
    val parkingLots: List<ParkingLot>
)

data class ParkingLot(
    val areaId: String,
    val areaName: String,
    val parkName: String,
    val totalSpace: Int

)