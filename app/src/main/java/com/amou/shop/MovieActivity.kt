package com.amou.shop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_movie.*
import kotlinx.android.synthetic.main.row_movie.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.info
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.net.URL

class MovieActivity : AppCompatActivity(), AnkoLogger {
    //產生 Retrofit 物件
    val retrofit = Retrofit.Builder()
            //基本網址
        .baseUrl("https://api.myjson.com/bins/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)
        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(this)

        doAsync {
            // 用 Retrofit 來取代
            //val json = URL("https://api.myjson.com/bins/ledyb").readText()
            //val movies = Gson().fromJson<List<Movie>>(json,
            //    object : TypeToken<List<Movie>>() {}.type )
            //產生建立的interface 物件
            val movieService = retrofit.create(MovieService::class.java)
            val movies = movieService.listMovies()
                .execute()
                .body()
            movies?.forEach {
                info("${it.Title}  ${it.imdbRating}")
            }
            uiThread {
                recycler.adapter = MovieAdapter(movies!!)

            }
        }

    }

    inner class MovieAdapter(val movies: List<Movie>) : RecyclerView.Adapter<MovieHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_movie, parent, false)
            return MovieHolder(view)
        }

        override fun getItemCount(): Int {
            return movies.size
        }

        override fun onBindViewHolder(holder: MovieHolder, position: Int) {
            //抓取 JSON 物件裡的資料 塞到holder 中
            holder.titleText.text = movies.get(position).Title
            holder.imdbText.text = movies.get(position).imdbRating
            holder.directorText.text = movies.get(position).Director
            Glide.with(this@MovieActivity)
                //抓取json 物件裡的網址圖片
                .load(movies.get(position).Poster)
                .override(300)
                .into(holder.posterImage)


        }

    }

    inner class MovieHolder(view: View) : RecyclerView.ViewHolder(view) {
        // 將UI view 元件指定給 Holder
        val titleText: TextView = view.text_movie_title
        val imdbText: TextView = view.text_movie_imdb
        val directorText: TextView = view.text_movie_director
        val posterImage: ImageView = view.image_movie_poster
    }
}

data class Movie(
    val Actors: String,
    val Awards: String,
    val ComingSoon: Boolean,
    val Country: String,
    val Director: String,
    val Genre: String,
    val Images: List<String>,
    val Language: String,
    val Metascore: String,
    val Plot: String,
    val Poster: String,
    val Rated: String,
    val Released: String,
    val Response: String,
    val Runtime: String,
    val Title: String,
    val Type: String,
    val Writer: String,
    val Year: String,
    val imdbID: String,
    val imdbRating: String,
    val imdbVotes: String,
    val totalSeasons: String
)

interface MovieService {
    //補上網址最後
    @GET("ledyb")
    fun listMovies(): Call<List<Movie>>
}


