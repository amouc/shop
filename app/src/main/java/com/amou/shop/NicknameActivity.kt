package com.amou.shop

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_nickname.*

class NicknameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nickname)
        button_done.setOnClickListener {
            //TODO nickname
            //將nickname 存在getSharedPreferences
            setNickname(ed_nickname.text.toString())

            //儲存資料到Firebase realtime database
            FirebaseDatabase.getInstance()
                .getReference("users")
                .child(FirebaseAuth.getInstance().currentUser!!.uid)
                .child("nickname")
                .setValue(ed_nickname.text.toString())
            setResult(Activity.RESULT_OK)
            finish()
        }
    }
}
