package com.amou.shop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_bus.*
import kotlinx.android.synthetic.main.row_bus.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.themedImageSwitcher
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class BusActivity : AppCompatActivity() {
    val retrofit = Retrofit.Builder()
        .baseUrl("https://data.tycg.gov.tw/opendata/datalist/datasetMeta/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bus)
        recycler_bus.setHasFixedSize(true)
        recycler_bus.layoutManager = LinearLayoutManager(this)
        doAsync {
            val busService = retrofit.create(BusService::class.java)
            val busdata = busService.listBus()
                .execute()
                .body()
            uiThread {
                recycler_bus.adapter = BusAdapter(busdata!!.datas)
            }

        }

    }


    inner class BusAdapter(val datas: List<Data>): RecyclerView.Adapter<BusHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_bus,parent,false)
            return BusHolder(view)

        }

        override fun getItemCount(): Int {
            return datas.size
        }

        override fun onBindViewHolder(holder: BusHolder, position: Int) {
            holder.busidText.text = datas.get(position).BusID
            holder.routeridText.text = datas.get(position).RouteID
            holder.speedText.text = datas.get(position).Speed

        }

    }

    inner class BusHolder(view:View): RecyclerView.ViewHolder(view){
        val busidText : TextView = view.text_busid
        val routeridText : TextView = view.textrouteid
        val speedText : TextView = view.text_speed

    }

}

data class Busdata(
    val datas: List<Data>
)

data class Data(
    val Azimuth: String,
    val BusID: String,
    val BusStatus: String,
    val DataTime: String,
    val DutyStatus: String,
    val GoBack: String,
    val Latitude: String,
    val Longitude: String,
    val ProviderID: String,
    val RouteID: String,
    val Speed: String,
    val ledstate: String,
    val sections: String
)

interface BusService{
    @GET("download?id=b3abedf0-aeae-4523-a804-6e807cbad589&rid=bf55b21a-2b7c-4ede-8048-f75420344aed")
    fun listBus():Call<Busdata>
}
